#include <petscsys.h>
#include <petscvec.h>
#include <petscmat.h>

PetscErrorCode FirstOrderUpwindRHS(Vec,Vec,void*,double);
PetscErrorCode SecondOrderCentralRHS(Vec,Vec,void*,double);
PetscErrorCode FifthOrderWENORHS(Vec,Vec,void*,double);

PetscErrorCode FifthOrderCRWENORHS(Vec,Vec,void*,double);
PetscErrorCode FifthOrderCRWENOLHS(Mat,void*,int,int);
