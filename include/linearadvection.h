#include <petscsys.h>
#include <petscdmda.h>
#include <petscvec.h>
#include <petscmat.h>
#include <petscksp.h>
#include <petscsnes.h>
#include <petscts.h>
#include <mpivars.h>
#include <weno.h>

/* Type Definitions */
typedef PetscReal 	    Real;
typedef PetscInt 	      Integer;
typedef PetscBool 	    Boolean;
typedef Real		        VarArray;
typedef PetscErrorCode	ErrorType;

typedef struct parameters_solver {
  /* Some basic options */
  Real		a;			      /* Advection speed                            */
  Real		xmin,xmax;		/* Domain limits                              */
  Integer	ic;			      /* Choice of initial conditions               */
  Integer	N;			      /* Number of grid points                      */
  Integer	T;			      /* Number of time steps                       */
  Real		tf;			      /* Final time                                 */
  Real		cfl,dx,dt;		/* Note: not inputs, these will be calculated */
  Integer	LeftB,RightB;	/* Left & right boundary indices (not inputs) */

  /* Algorithm options */
  Integer	spatial_scheme;	/* Choice of spatial reconstruction scheme  */
  /* PETSC handles the time integration, so commented out */
  /*Integer	time_scheme;*/		/* Choice of time-marching scheme       */
  Real space_order;    /* Theoretical order of reconstruction scheme   */
  Real time_order;     /* Theoretical order of time-integration scheme */
  /* space_order and time_order are used only to calculate refinement     */
  /* factors during convergence analysis. They do not affect the main     */
  /* solution process.                                                    */


  /* IO options */
  Boolean	write_initial;		  /* Write the initial solution to a file?  */
  Boolean	write_exact;		    /* Write the exact solution to a file?    */
  Boolean	write_final;		    /* Write the final solution to a file?    */
  char		initial_fname[256];	/* Filename for initial solution          */
  char		exact_fname[256];	  /* Filename for initial solution          */
  char		final_fname[256];	  /* Filename for final solution            */

  MPIVariables   MPIVars;	    /* contains MPI related variables         */
  WENOParameters weno;        /* WENO algorithm related options         */

  /* Function pointers to calculating right and left hand sides  of the reconstruction process	*/
  /* This code uses an unified framework for compact and non-compact schemes 		              	*/
  /* Reconstruction of the function derivative can be expressed as [A]f_x = [B]f 		            */
  /* For non-compact reconstruction schemes, [A] = [I] (identity matrix)		                  	*/
  ErrorType (*ReconstructionRHS)(Vec,Vec,void*,Real); /* Computes [B]u (u is the input vector)  */
  ErrorType (*ReconstructionLHS)(Mat,void*,int,int);	/* Computes [A]                           */

  Real  shift; /* Variable used in PETSc's Jacobian calculations          */
  /* If spatial scheme is compact, then the following variables are used  */
  Vec   Bf;
  Mat   A;
  KSP   ksp;

} SolverParameters;

