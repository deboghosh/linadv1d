#include <petscsys.h>
#include <petscvec.h>
typedef struct parameters_weno {
  /* Options related to the type of WENO scheme */
  PetscBool mapped;		    /* Use mapped weights?                                    */
  PetscBool borges;		    /* Use Borges' implementation of weights?                 */
  PetscBool yc;		        /* Use Yamaleev-Carpenter implementation of weights?      */
  PetscBool no_limiting;  /* Remove limiting -> 5th order polynomial interpolation  */
  double		eps;		      /* epsilon parameter                                      */
  double		p;			      /* p parameter                                            */

  Vec       w1,w2,w3;     /* Arrays containing the WENO weights for a given time-   */
                          /* integration step/stage                                 */
} WENOParameters;
