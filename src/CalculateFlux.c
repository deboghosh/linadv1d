#include <stdio.h>
#include <linearadvection.h>

ErrorType CalculateFlux(DM da,Vec U, Vec f,void* S)
{
  if (!S) {
    printf("Error: in CalculateFlux() - NULL pointer ");
    printf("passed as input for solver parameters.\n" );
    return(1);
  }
  SolverParameters  *p = (SolverParameters*)S;
  DMDALocalInfo		  da_info;
  DMDAGetLocalInfo(da,&da_info);

  Vec ULoc;
  DMGetLocalVector(da,&ULoc);
  VecZeroEntries(ULoc);
  DMGlobalToLocalBegin(da,U,INSERT_VALUES,ULoc);
  DMGlobalToLocalEnd  (da,U,INSERT_VALUES,ULoc);

  VarArray *uarr, *farr;
  DMDAVecGetArray(da,ULoc,&uarr);
  DMDAVecGetArray(da,f   ,&farr);

  /* Flux needs to be computed at the ghost points too */
  Integer is = da_info.gxs;
  Integer ie = da_info.gxs + da_info.gxm;
  Real     a = p->a;

  Integer i;
  for (i=is; i<ie; i++) {
    farr[i] = a*uarr[i];
  }

  DMDAVecRestoreArray(da,ULoc,&uarr);
  DMDAVecRestoreArray(da,f   ,&farr);
  DMRestoreLocalVector(da,&ULoc);
  return(0);
}
