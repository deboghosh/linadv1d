#include <stdio.h>
#include <math.h>
#include <linearadvection.h>

Real absolute(Real);
const Real pi = 4.0*atan(1.0);

ErrorType SetSolution(DM da, Vec U, void* S, Real t)
{
  if (!S) {
    printf("Error: in SetSolution() - solver parameter is a NULL pointer.\n");
    return(1);
  }
  SolverParameters	*params = (SolverParameters*) S;

  /* extracting local info and vector */
  DMDALocalInfo	info;
  DMDAGetLocalInfo(da,&info);
  Integer istart,iend;
  istart = info.xs;
  iend = info.xs + info.xm;

  VarArray	*u;
  DMDAVecGetArray(da,U,&u);

  /* calculating dx */
  Real dx, xmin,xmax,length;
  xmin 		= params->xmin;
  xmax 		= params->xmax;
  length	= xmax - xmin;
  dx 		= length / (Real)info.mx;
  params->dx 	= dx;

  if (params->ic == 1) {
    Integer i;
    for (i=istart; i<iend; i++) {
      Real x = (xmin + i*dx) - params->a*t;
      u[i] = sin(2.0*pi*x);
    }
  }else if (params->ic == -1) {
    Integer i;
    for (i=istart; i<iend; i++) {
      Real x = (xmin + i*dx) - params->a*t;
      if (x <= -0.8) {
        u[i] = 0;
      } else if (x <= -0.6) {
        u[i] = exp(-log(2.0)*(x+0.7)*(x+0.7)/0.0009);
      } else if (x <= -0.4) {
        u[i] = 0;
      } else if (x <= -0.2) {
        u[i] = 1.0;
      } else if (x <= 0) {
        u[i] = 0;
      } else if (x <= 0.2) {
        u[i] = 1-absolute(10*(x-0.1));
      } else if (x <= 0.4) {
        u[i] = 0;
      } else if (x <= 0.6) {
        u[i] = sqrt(1-100*(x-0.5)*(x-0.5));
      } else {
        u[i] = 0;
      }
    }
  }else if (params->ic == -2) {
    Integer i;
    for (i=istart; i<iend; i++) {
      Real x = (xmin + i*dx) - params->a*t;
      if (x <= 0.4) {
        u[i] = 0;
      } else if (x <= 0.6) {
        u[i] = 1.0;
      } else {
        u[i] = 0;
      }
    }
  }else{
    if (!params->MPIVars.rank) printf("Error (SetSolution): Invalid choice of initial conditions.\n");
    return(1);
  }

  DMDAVecRestoreArray(da,U,&u);
  return(0);
}
