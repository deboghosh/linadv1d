#include <stdio.h>
#include <math.h>
#include <time.h>
#include <constants.h>
#include <solutionmethods.h>
#include <linearadvection.h>

ErrorType SetSolution(DM,Vec,void*,Real);
ErrorType InitialSetup(void*);
ErrorType PetscRHSFunction(TS,Real,Vec,Vec,void*);
ErrorType PetscIFunction(TS,Real,Vec,Vec,Vec,void*);
ErrorType PetscIJacobian(TS,Real,Vec,Vec,Real,Mat*,Mat*,MatStructure*,void*);
ErrorType PetscJacobianFunction(Mat,Vec,Vec);

ErrorType LinearAdvection(void* S, Real* err, Real* waqt)
{
  if (!S) {
    printf("Error: in LinearAdvection() - solver parameter S is a NULL pointer.\n");
    return(1);
  }
  SolverParameters *solver = (SolverParameters*) S;
  DM	da;
  Vec	u, u_exact;
  ErrorType ierr;
  Mat A;

  /* Allocate and initialize solution vector and exact solution */
  DMDACreate1d(PETSC_COMM_WORLD,DMDA_BOUNDARY_PERIODIC,solver->N,_NDOF_,_GHOSTSIZE_,PETSC_NULL,&da);
  DMCreateGlobalVector(da,&u);
  DMCreateGlobalVector(da,&u_exact);
  ierr = SetSolution(da,u,solver,0);
  ierr = SetSolution(da,u_exact,solver,solver->tf);
  if (ierr)	return(ierr);
  ierr = InitialSetup(solver);
  if (ierr)	return(ierr);

  DMDALocalInfo info;
  DMDAGetLocalInfo(da,&info);
  Integer size = info.xm;

  if (!solver->MPIVars.rank) {
    printf("Linear Advection: N = %d\tT = %d\tCFL = %f\n",solver->N,solver->T,solver->cfl);
    printf("Linear Advection: dx = %f\tdt = %f\n",solver->dx,solver->dt);
  }

  /* Write initial solution to file */
  if (solver->write_initial) {
    PetscViewer writer;
    PetscViewerASCIIOpen(PETSC_COMM_WORLD,solver->initial_fname,&writer);
    PetscViewerSetFormat(writer,PETSC_VIEWER_ASCII_SYMMODU);
    VecView(u,writer);
    PetscViewerDestroy(&writer);
  }
  /* Write exact solution to file */
  if (solver->write_exact) {
    PetscViewer writer;
    PetscViewerASCIIOpen(PETSC_COMM_WORLD,solver->exact_fname,&writer);
    PetscViewerSetFormat(writer,PETSC_VIEWER_ASCII_SYMMODU);
    VecView(u_exact,writer);
    PetscViewerDestroy(&writer);
  }

  /* Set up time-integration and solve */
  TS ts;
  TSCreate(PETSC_COMM_WORLD,&ts);
  TSSetDM(ts,da);

  TSSetFromOptions(ts);
  TSSetDuration(ts,solver->T,solver->tf);
  TSSetInitialTimeStep(ts,0.0,solver->dt);
  TSSetExactFinalTime(ts,TS_EXACTFINALTIME_MATCHSTEP);

  TSType time_scheme;
  TSGetType(ts,&time_scheme);

  /* Allocate arrays for WENO weights */
  if ((solver->spatial_scheme == _FIFTH_ORDER_WENO_) || (solver->spatial_scheme == _FIFTH_ORDER_CRWENO_)) {
    DMCreateLocalVector(da,&solver->weno.w1);
    DMCreateLocalVector(da,&solver->weno.w2);
    DMCreateLocalVector(da,&solver->weno.w3);
  }
  /* Allocate matrix, vectors and KSP for compact reconstruction scheme */
  if (solver->spatial_scheme == _FIFTH_ORDER_CRWENO_) {
    MatCreate(PETSC_COMM_WORLD,&solver->A);
    MatSetSizes(solver->A,size,size,solver->N,solver->N);
    MatSetType(solver->A,MATAIJ);
    MatSeqAIJSetPreallocation(solver->A,3,NULL);
    MatMPIAIJSetPreallocation(solver->A,3,NULL,1,NULL);
    MatSetDM(solver->A,da);
    MatSetUp(solver->A);
    if ((!strcmp(time_scheme,TSEULER)) || (!strcmp(time_scheme,TSSSP))){
      DMCreateGlobalVector(da,&solver->Bf);
      KSPCreate(PETSC_COMM_WORLD,&solver->ksp);
      KSPSetFromOptions(solver->ksp);
    }
  }

  /* Define the left and right hand side functions for time-integration */
  if ((!strcmp(time_scheme,TSEULER)) 
      || (!strcmp(time_scheme,TSSSP))
      || (!strcmp(time_scheme,TSRK))){

    /* Exlicit time-integration schemes */
    TSSetRHSFunction(ts,PETSC_NULL,PetscRHSFunction,solver);

  } else if ((!strcmp(time_scheme,TSBEULER)) || (!strcmp(time_scheme,TSARKIMEX))) {

    /* Implicit time-integration schemes */
    TSSetIFunction(ts,PETSC_NULL,PetscIFunction,solver);
    MatCreateShell(PETSC_COMM_WORLD,size,size,PETSC_DETERMINE,PETSC_DETERMINE,solver,&A);
    MatShellSetOperation(A,MATOP_MULT,(void (*)(void))PetscJacobianFunction);
    MatSetUp(A);
    TSSetIJacobian(ts,A,A,PetscIJacobian,solver);

    /* Set pre-conditioner to none for MatShell */
    SNES  snes;
    KSP   ksp;
    PC    pc;
    TSGetSNES(ts,&snes);
    SNESGetKSP(snes,&ksp);
    KSPGetPC(ksp,&pc);
    PCSetType(pc,PCNONE);

    if (!strcmp(time_scheme,TSARKIMEX)) {
      TSSetEquationType(ts,TS_EQ_DAE_IMPLICIT_INDEX1);
      TSARKIMEXSetFullyImplicit(ts,PETSC_TRUE);
    }
  } else {

    if (!solver->MPIVars.rank) printf("Error in LinearAdvection: Unknown time_scheme.\n");
    return(1);

  }

  TSSetSolution(ts,u);

  clock_t start_time = clock();
  TSSolve(ts,u);
  clock_t end_time   = clock();
  clock_t total_time = end_time - start_time;
  *waqt = (Real) total_time / (Real) CLOCKS_PER_SEC;

  /* Write final solution to file */
  if (solver->write_final) {
    PetscViewer writer;
    PetscViewerASCIIOpen(PETSC_COMM_WORLD,solver->final_fname,&writer);
    PetscViewerSetFormat(writer,PETSC_VIEWER_ASCII_SYMMODU);
    VecView(u,writer);
    PetscViewerDestroy(&writer);
  }

  /* Calculate errors */
  ierr = VecAYPX(u_exact,-1.0,u);
  VecNorm(u_exact,NORM_1,&err[0]); err[0] /= solver->N;
  VecNorm(u_exact,NORM_2,&err[1]); err[1] = sqrt((err[1]*err[1])/solver->N);
  VecNorm(u_exact,NORM_INFINITY,&err[2]);

  /* Clean up */
  if ((solver->spatial_scheme == _FIFTH_ORDER_WENO_) || (solver->spatial_scheme == _FIFTH_ORDER_CRWENO_)) {
    VecDestroy(&solver->weno.w1);
    VecDestroy(&solver->weno.w2);
    VecDestroy(&solver->weno.w3);
  }
  if (solver->spatial_scheme == _FIFTH_ORDER_CRWENO_) {
    MatDestroy(&solver->A);
    if ((!strcmp(time_scheme,TSEULER)) || (!strcmp(time_scheme,TSSSP))){
      VecDestroy(&solver->Bf);
      KSPDestroy(&solver->ksp);
    }
  }
  if ((!strcmp(time_scheme,TSBEULER)) || (!strcmp(time_scheme,TSARKIMEX))) {
    MatDestroy(&A);
  }
  TSDestroy(&ts);
  VecDestroy(&u);
  VecDestroy(&u_exact);
  DMDestroy(&da);

  if (!solver->MPIVars.rank) {
    printf("Linear Advection: Runtime = %f\n",*waqt);
  }
  return(0);
}
