#include <stdio.h>
#include <petscsys.h>
#include <petscdmda.h>
#include <petscvec.h>

/* Calculate R = (1/dx)*[f_x] using first order upwind  */
/* R is a global vector, while F is a local vector      */
PetscErrorCode FirstOrderUpwindRHS(Vec F, Vec R, void* S,double dx)
{
  int		  i, is, ie;
  Vec		  fI;
  double  *f,*fi,*r;

  DM da;
  VecGetDM(F,&da);
  DMDALocalInfo da_info;
  DMDAGetLocalInfo(da,&da_info);
  is = da_info.xs;
  ie = da_info.xs + da_info.xm;

  /* Calculate interface flux */
  DMCreateLocalVector(da,&fI);
  DMDAVecGetArray(da,F,&f);
  DMDAVecGetArray(da,fI,&fi);
  for (i=is; i<ie+1; i++) {
    fi[i] = f[i-1];
  }
  DMDAVecRestoreArray(da,F,&f);

  /* Calculating cell-centered flux derivative */
  double dxinv = 1.0/dx;
  DMDAVecGetArray(da,R,&r);
  for (i=is; i<ie; i++) {
    r[i] = -dxinv * (fi[i+1] - fi[i]);
  }
  DMDAVecRestoreArray(da,fI,&fi);
  DMDAVecRestoreArray(da,R,&r);
  VecDestroy(&fI);

  return(0);
}
