#include <stdio.h>
#include <constants.h>
#include <solutionmethods.h>
#include <linearadvection.h>

ErrorType InitialSetup(void *parameters)
{
  if (!parameters) {
    printf("Error: in InitialSetup() - solver parameter is a NULL pointer.\n");
    return(1);
  }
  SolverParameters *p = (SolverParameters*) parameters;
  p->dt = p->tf / p->T;
  p->cfl = p->a*p->dt / p->dx;

  p->LeftB  = 0;
  p->RightB = p->N-1;

  if (p->spatial_scheme == _FIRST_ORDER_UPWIND_) {
    p->ReconstructionRHS = FirstOrderUpwindRHS;
    p->ReconstructionLHS = NULL;
  } else if (p->spatial_scheme == _SECOND_ORDER_CENTRAL_) {
    p->ReconstructionRHS = SecondOrderCentralRHS;
    p->ReconstructionLHS = NULL;
  } else if (p->spatial_scheme == _FIFTH_ORDER_WENO_) {
    p->ReconstructionRHS = FifthOrderWENORHS;
    p->ReconstructionLHS = NULL;
  } else if (p->spatial_scheme == _FIFTH_ORDER_CRWENO_) {
    p->ReconstructionRHS = FifthOrderCRWENORHS;
    p->ReconstructionLHS = FifthOrderCRWENOLHS;
  } else {
    printf("Error: in InitialSetup - invalid choice of spatial_scheme.\n");
    return(1);
  }
  return(0);
}
