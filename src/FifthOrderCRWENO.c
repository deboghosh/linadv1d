#include <stdio.h>
#include <petscsys.h>
#include <petscdmda.h>
#include <petscvec.h>
#include <petscmat.h>
#include <constants.h>
#include <weno.h>

/* Calculate R = (1/dx)*[f_x] using fifth order CRWENO  */
PetscErrorCode FifthOrderCRWENORHS(Vec F, Vec R, void* S, double dx)
{
  if (!S) {
    printf("Error: in FifthOrderCRWENORHS() - NULL pointer ");
    printf("passed as input for WENO parameters.\n"         );
    return(1);
  }
  WENOParameters	*p = (WENOParameters*) S;
  int		  i, is, ie;
  Vec			fI;
  double  *f,*fi,*r;
  double  *w1,*w2,*w3;

  DM da;
  VecGetDM(F,&da);
  DMDALocalInfo da_info;
  DMDAGetLocalInfo(da,&da_info);
  is = da_info.xs;
  ie = da_info.xs + da_info.xm;

  /* Calculate interface flux */
  DMCreateLocalVector(da,&fI);
  DMDAVecGetArray(da,F,&f);
  DMDAVecGetArray(da,fI,&fi);
  DMDAVecGetArray(da,p->w1,&w1);
  DMDAVecGetArray(da,p->w2,&w2);
  DMDAVecGetArray(da,p->w3,&w3);
  for (i=is; i<ie+1; i++) {
    /* Defining stencil points */
    double m2, m1, p1;
    m2 = f[i-2];
    m1 = f[i-1];
    p1 = f[i];

    /* Candidate stencils */
    double f1, f2, f3;
    f1 = one_sixth * (m1 + 5*p1);
    f2 = one_sixth * (5*m1 + p1);
    f3 = one_sixth * (m2 + 5*m1);

    fi[i] = w1[i]*f1 + w2[i]*f2 + w3[i]*f3;
  }
  DMDAVecRestoreArray(da,F,&f);
  DMDAVecRestoreArray(da,p->w1,&w1);
  DMDAVecRestoreArray(da,p->w2,&w2);
  DMDAVecRestoreArray(da,p->w3,&w3);

  /* Calculating cell-centered flux derivative */
  double dxinv = 1.0/dx;
  DMDAVecGetArray(da,R,&r);
  for (i=is; i<ie; i++) {
    r[i] = -dxinv * (fi[i+1] - fi[i]);
  }
  DMDAVecRestoreArray(da,fI,&fi);
  DMDAVecRestoreArray(da,R,&r);
  VecDestroy(&fI);

  return(0);
}

PetscErrorCode FifthOrderCRWENOLHS(Mat A, void *S, int LeftB, int RightB)
{
  WENOParameters    *p = (WENOParameters*) S;
  if (!p) {
    printf("Error: in FifthOrderCRWENOLHS() - NULL pointer passed ");
    printf("as argument for WENO parameters.\n"                    );
    return(1);
  }
  int	   i, is, ie;
  Vec		 tridiag_A,tridiag_B,tridiag_C;
  double *a,*b,*c;
  double *w1,*w2,*w3;

  DM da;
  MatGetDM(A,&da);
  DMDALocalInfo da_info;
  DMDAGetLocalInfo(da,&da_info);
  is  = da_info.xs;
  ie  = da_info.xs + da_info.xm;

  /* Arrays to store the tridiagonal coefficients */
  DMCreateLocalVector(da,&tridiag_A);
  DMCreateLocalVector(da,&tridiag_B);
  DMCreateLocalVector(da,&tridiag_C);
  DMDAVecGetArray(da,tridiag_A,&a);
  DMDAVecGetArray(da,tridiag_B,&b);
  DMDAVecGetArray(da,tridiag_C,&c);

  DMDAVecGetArray(da,p->w1,&w1);
  DMDAVecGetArray(da,p->w2,&w2);
  DMDAVecGetArray(da,p->w3,&w3);
  for (i=is; i<ie+1; i++) {
    a[i] = two_third*w3[i] + one_third*w2[i];
    b[i] = one_third*w3[i] + two_third*(w2[i]+w1[i]);
    c[i] = one_third*w1[i];
  }
  DMDAVecRestoreArray(da,p->w1,&w1);
  DMDAVecRestoreArray(da,p->w2,&w2);
  DMDAVecRestoreArray(da,p->w3,&w3);

  /* Calculating A */
  MatZeroEntries(A);
  int     matis, matie;
  int     col[3];
  double  value[3];
  MatGetOwnershipRange(A,&matis,&matie);
  for (i=matis; i<matie; i++) {
    double alpha, beta, gamma;
    alpha = a[i];
    beta  = a[i]+b[i]-a[i+1];
    gamma = a[i]+b[i]+c[i]-a[i+1]-b[i+1];

    if (i == LeftB) {
      value[0] = alpha; col[0] = RightB;
      value[1] = beta;  col[1] = i;
      value[2] = gamma; col[2] = i+1;
    } else if (i == RightB) {
      value[0] = alpha; col[0] = i-1;
      value[1] = beta;  col[1] = i;
      value[2] = gamma; col[2] = LeftB;
    } else {
      value[0] = alpha; col[0] = i-1;
      value[1] = beta;  col[1] = i;
      value[2] = gamma; col[2] = i+1;
    }
    MatSetValues(A,1,&i,3,col,value,INSERT_VALUES);
  }
  MatAssemblyBegin(A,MAT_FINAL_ASSEMBLY);
  MatAssemblyEnd  (A,MAT_FINAL_ASSEMBLY);

  DMDAVecRestoreArray(da,tridiag_A,&a);
  DMDAVecRestoreArray(da,tridiag_B,&b);
  DMDAVecRestoreArray(da,tridiag_C,&c);
  VecDestroy(&tridiag_A);
  VecDestroy(&tridiag_B);
  VecDestroy(&tridiag_C);

  return(0);
}
