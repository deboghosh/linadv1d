#include <stdio.h>
#include <petscsys.h>
#include <petscdmda.h>
#include <petscvec.h>
#include <constants.h>
#include <weno.h>

double absolute(double);
double raiseto(double,double);

/* Calculate R = (1/dx)*[f_x] using fifth order WENO    */
/* R is a global vector, while F is a local vector      */
PetscErrorCode FifthOrderWENORHS(Vec F, Vec R, void* S, double dx)
{
  if (!S) {
    printf("Error: in FifthOrderWENORHS() - NULL pointer ");
    printf("passed as input for WENO parameters.\n"       );
    return(1);
  }
  WENOParameters	*p = (WENOParameters*) S;
  int     i, is, ie;
  Vec		  fI;
  double  *f,*fi,*r;
  double  *w1,*w2,*w3;

  DM da;
  VecGetDM(F,&da);
  DMDALocalInfo da_info;
  DMDAGetLocalInfo(da,&da_info);
  is = da_info.xs;
  ie = da_info.xs + da_info.xm;

  /* Calculate interface flux */
  DMCreateLocalVector(da,&fI);
  DMDAVecGetArray(da,F,&f);
  DMDAVecGetArray(da,fI,&fi);
  DMDAVecGetArray(da,p->w1,&w1);
  DMDAVecGetArray(da,p->w2,&w2);
  DMDAVecGetArray(da,p->w3,&w3);
  for (i=is; i<ie+1; i++) {
    /* Defining stencil points */
    double m3, m2, m1, p1, p2;
    m3 = f[i-3];
    m2 = f[i-2];
    m1 = f[i-1];
    p1 = f[i];
    p2 = f[i+1];

    /* Candidate stencils */
    double f1, f2, f3;
    f1 = (2*one_sixth)*m1 + (5*one_sixth)*p1 - (one_sixth)*p2;
    f2 = (-one_sixth)*m2 + (5.0*one_sixth)*m1 + (2*one_sixth)*p1;
    f3 = (2*one_sixth)*m3 - (7.0*one_sixth)*m2 + (11.0*one_sixth)*m1;

    fi[i] = w1[i]*f1 + w2[i]*f2 + w3[i]*f3;
  }
  DMDAVecRestoreArray(da,F,&f);
  DMDAVecRestoreArray(da,p->w1,w1);
  DMDAVecRestoreArray(da,p->w2,w2);
  DMDAVecRestoreArray(da,p->w3,w3);
  
  /* Calculating cell-centered flux derivative */
  double dxinv = 1.0/dx;
  DMDAVecGetArray(da,R,&r);
  for (i=is; i<ie; i++) {
    r[i] = -dxinv * (fi[i+1] - fi[i]);
  }
  DMDAVecRestoreArray(da,fI,&fi);
  DMDAVecRestoreArray(da,R,&r);
  VecDestroy(&fI);

  return(0);
}
