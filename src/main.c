static const char help[] = "Linear advection equation in 1D over a periodic domain\n";

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <linearadvection.h>

Real order(Integer);
ErrorType LinearAdvection(void*,Real*,Real*);

int main(int argc, char **argv)
{
  PetscInitialize(&argc,&argv,(char *)0,help);
  SolverParameters	solver;
  Integer		space_levels;	/* number of refinement levels for spatial convergence          */
  Integer		time_levels;	/* number of refinement levels for temporal convergence         */
  Integer		N_space;	    /* initial number of grid points for spatial convergence test   */
  Integer		T_space;	    /* initial number of time steps  for spatial convergence test   */
  Integer		N_time;		    /* initial number of grid points for temporal convergence test  */
  Integer		T_time;		    /* initial number of time steps  for temporal convergence test  */
  Real      space_refine; /* refinement factor for spatial convergence test               */
  Real      time_refine;  /* refinement factor for temporal convergence test              */
  Integer		i;		        /* Loop index */
  ErrorType	ierr;	  	    /* Error flag */

#ifdef noMPI
  solver.MPIVars.rank = 0;
  solver.MPIVars.nproc = 1;
#else
  MPI_Comm_rank(PETSC_COMM_WORLD,&solver.MPIVars.rank);
  MPI_Comm_size(PETSC_COMM_WORLD,&solver.MPIVars.nproc);
#endif

  /* Convergence analysis options */
  space_levels  = 1;
  time_levels   = 0;
  N_space	      = 100;
  T_space       = 100;
  N_time        = 100;
  T_time        = 100;
  space_refine  = 2.0;
  time_refine   = 2.0;
  PetscOptionsInt("-space-levels","Number of refinement levels for spatial convergence test","<1>",
                  space_levels,&space_levels,PETSC_NULL);
  PetscOptionsInt("-time-levels","Number of refinement levels for spatial convergence test","<0>",
                  time_levels,&time_levels,PETSC_NULL);
  PetscOptionsInt("-grid-size-space","Number of grid points for first refinement level","<100>",
                  N_space,&N_space,PETSC_NULL);
  PetscOptionsInt("-time-steps-space","Number of time steps for first refinement level","<100>",
                  T_space,&T_space,PETSC_NULL);
  PetscOptionsInt("-grid-size-time","Number of grid points for first refinement level","<100>",
                  N_time,&N_time,PETSC_NULL);
  PetscOptionsInt("-time-steps-time","Number of time steps for first refinement level","<100>",
                  T_time,&T_time,PETSC_NULL);
  PetscOptionsReal("-space-refine-factor","Refinement factor for spatial convergence test","<2.0>",
                  space_refine,&space_refine,PETSC_NULL);
  PetscOptionsReal("-time-refine-factor","Refinement factor for temporal convergence test","<2.0>",
                  time_refine,&time_refine,PETSC_NULL);

  /* Basic options */
  solver.a			= 1.0; 
  solver.xmin		= 0.0; 
  solver.xmax		= 1.0; 
  solver.ic			= 1;
  solver.tf			= 1.0;
  PetscOptionsReal("-a","Advection rate","<1.0>",solver.a,&solver.a,PETSC_NULL);
  PetscOptionsReal("-xmin","Domain xmin","<0.0>",solver.xmin,&solver.xmin,PETSC_NULL);
  PetscOptionsReal("-xmax","Domain xmax","<1.0>",solver.xmax,&solver.xmax,PETSC_NULL);
  PetscOptionsInt ("-initial","Initial solution","<1 (Sine wave)>",solver.ic,&solver.ic,PETSC_NULL);
  PetscOptionsReal("-final-time","Final time","<1.0>",solver.tf,&solver.tf,PETSC_NULL);
 
  /* Algorithm options */
  solver.spatial_scheme	= 1;
  PetscOptionsInt ("-reconstruction-scheme","Reconstruction Scheme","",solver.spatial_scheme,&solver.spatial_scheme,PETSC_NULL);
  PetscOptionsReal("-space-order","Order of reconstruction scheme","",solver.space_order,&solver.space_order,PETSC_NULL);
  PetscOptionsReal("-time-order","Order of time-integration scheme","",solver.time_order,&solver.time_order,PETSC_NULL);

  /* IO options */
  strcpy(solver.initial_fname,"initial_solution.dat");
  strcpy(solver.exact_fname,"initial_solution.dat");
  strcpy(solver.final_fname,"final_solution.dat");
  PetscOptionsString("-write-initial","Write initial solution to given file name","",
                     solver.initial_fname,solver.initial_fname,sizeof solver.initial_fname,&solver.write_initial);
  PetscOptionsString("-write-exact","Write exact solution to given file name","",
                     solver.exact_fname,solver.exact_fname,sizeof solver.exact_fname,&solver.write_exact);
  PetscOptionsString("-write-final","Write final solution to given file name","",
                     solver.final_fname,solver.final_fname,sizeof solver.final_fname,&solver.write_final);

  /* WENO options */
  solver.weno.borges		  = PETSC_FALSE;
  solver.weno.yc		      = PETSC_FALSE;
  solver.weno.no_limiting	= PETSC_FALSE;
  solver.weno.mapped		  = PETSC_FALSE;
  solver.weno.eps		      = 1e-6;
  solver.weno.p		        = 2;
  PetscOptionsBool("-weno-options-use-borges","Use Borges' WENO weights?","",
                   solver.weno.borges,&solver.weno.borges,PETSC_NULL);
  PetscOptionsBool("-weno-options-use-yc","Use Yamaleev-Carpenter WENO weights?","",
                   solver.weno.yc,&solver.weno.yc,PETSC_NULL);
  PetscOptionsBool("-weno-options-use-mapped","Use Mapped WENO weights?","",
                   solver.weno.mapped,&solver.weno.mapped,PETSC_NULL);
  PetscOptionsBool("-weno-options-use-no-limiting","Use fith order polynomial interpolation?","",
                   solver.weno.no_limiting,&solver.weno.no_limiting,PETSC_NULL);
  PetscOptionsReal("-weno-options-epsilon","Value of epsilon parameter in WENO scheme","",
                   solver.weno.eps,&solver.weno.eps,PETSC_NULL);
  PetscOptionsReal("-weno-options-p","Value of p parameter in WENO scheme","",
                   solver.weno.p,&solver.weno.p,PETSC_NULL);


  Real    *errL1, *errL2, *errL8, *waqt;
  Integer *grid, *iter;
  FILE    *out;

  /* Spatial convergence analysis */
  errL1	= (Real*) calloc (space_levels,sizeof(Real)); 		    /* array to store L1 error              */
  errL2	= (Real*) calloc (space_levels,sizeof(Real)); 		    /* array to store L2 error              */
  errL8	= (Real*) calloc (space_levels,sizeof(Real)); 		    /* array to store L_infinity error      */
  waqt	= (Real*) calloc (space_levels,sizeof(Real)); 		    /* array to store runtime               */
  grid	= (Integer*) calloc(space_levels,sizeof(Integer));   	/* array to store grid sizes            */
  iter	= (Integer*) calloc(space_levels,sizeof(Integer));  	/* array to store number of time steps  */
  out   = fopen("err_space.dat","w");
  for (i = 0; i < space_levels; i++)
  {
    printf("\nSpatial convergence: Refinement level %d of %d.\n",i+1,space_levels);
    if (i == 0)	grid[i] = N_space;
    else	grid[i] = space_refine*grid[i-1];
    solver.N	= grid[i];

    if (i == 0)	iter[i] = T_space;
    else {
      Real refine_factor = solver.space_order/solver.time_order;
      if (refine_factor < 1.0)  refine_factor = 1.0;
      iter[i] = iter[i-1]*exp(refine_factor*log(space_refine));
    }
    solver.T	= iter[i];

    Real errors[3];
    ierr = LinearAdvection(&solver,&errors[0],&waqt[i]);
    if (ierr) {
      printf("Error: LinearAdvection exited with error in rank %d. Aborting execution.", solver.MPIVars.rank);
      return(0);
    }
    errL1[i] = errors[0];
    errL2[i] = errors[1];
    errL8[i] = errors[2];
    Real convL1, convL2, convL8;
    if (i > 0) {
      convL1 = - (log(errL1[i]) - log(errL1[i-1])) / (log(grid[i]) - log(grid[i-1]));
      convL2 = - (log(errL2[i]) - log(errL2[i-1])) / (log(grid[i]) - log(grid[i-1]));
      convL8 = - (log(errL8[i]) - log(errL8[i-1])) / (log(grid[i]) - log(grid[i-1]));
      if (!solver.MPIVars.rank) printf("Spatial convergence rates: L1 = %f, \tL2 = %f, \tL_inf = %f\n", convL1, convL2, convL8);
    }else{
      convL1 = 0;
      convL2 = 0;
      convL8 = 0;
    }
    fprintf(out, "%f\t%E\t%f\t%E\t%f\t%E\t%f\t%E\n",(solver.xmax-solver.xmin)/grid[i],errL1[i],convL1,errL2[i],convL2,errL8[i],convL8,waqt[i]);
  }
  fclose(out);
  free(errL1);
  free(errL2);
  free(errL8);
  free(waqt);
  free(grid);
  free(iter);

  /* Time convergence analysis */
  errL1	= (Real*) calloc (time_levels,sizeof(Real)); 		    /* array to store L1 error              */
  errL2	= (Real*) calloc (time_levels,sizeof(Real)); 		    /* array to store L2 error              */
  errL8	= (Real*) calloc (time_levels,sizeof(Real)); 		    /* array to store L_infinity error      */
  waqt	= (Real*) calloc (time_levels,sizeof(Real)); 		    /* array to store runtime               */
  grid	= (Integer*) calloc(time_levels,sizeof(Integer)); 	/* array to store grid sizes            */
  iter	= (Integer*) calloc(time_levels,sizeof(Integer));  	/* array to store number of time steps  */
  out   = fopen("err_time.dat","w");
  for (i = 0; i < time_levels; i++)
  {
    printf("\nTime convergence: Refinement level %d of %d.\n",i+1,time_levels);
    if (i == 0)	iter[i] = T_time;
    else	iter[i] = time_refine*iter[i-1];
    solver.T	= iter[i];

    if (i == 0)	grid[i] = N_time;
    else {
      Real refine_factor = solver.time_order/solver.space_order;
      if (refine_factor < 1.0)  refine_factor = 1.0;
      grid[i] = grid[i-1]*exp(refine_factor*log(time_refine));
    }
    solver.N	= grid[i];

    Real errors[3];
    ierr = LinearAdvection(&solver,&errors[0],&waqt[i]);
    if (ierr) {
      printf("Error: LinearAdvection exited with error in rank %d. Aborting execution.", solver.MPIVars.rank);
      return(0);
    }
    errL1[i] = errors[0];
    errL2[i] = errors[1];
    errL8[i] = errors[2];
    Real convL1, convL2, convL8;
    if (i > 0) {
      convL1 = - (log(errL1[i]) - log(errL1[i-1])) / (log(iter[i]) - log(iter[i-1]));
      convL2 = - (log(errL2[i]) - log(errL2[i-1])) / (log(iter[i]) - log(iter[i-1]));
      convL8 = - (log(errL8[i]) - log(errL8[i-1])) / (log(iter[i]) - log(iter[i-1]));
      if (!solver.MPIVars.rank) printf("Time convergence rates: L1 = %f, \tL2 = %f, \tL_inf = %f\n", convL1, convL2, convL8);
    } else {
      convL1 = 0;
      convL2 = 0;
      convL8 = 0;
    }
    fprintf(out, "%f\t%E\t%f\t%E\t%f\t%E\t%f\t%E\n",solver.tf/iter[i],errL1[i],convL1,errL2[i],convL2,errL8[i],convL8,waqt[i]);
  }
  fclose(out);
  free(errL1);
  free(errL2);
  free(errL8);
  free(waqt);
  free(grid);
  free(iter);

  PetscFinalize();
  return(0);
}
