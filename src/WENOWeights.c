#include <stdio.h>
#include <petscdmda.h>
#include <petscvec.h>

#include <weno.h>
#include <constants.h>
#include <solutionmethods.h>

double absolute(double);
double raiseto(double,double);

/* Calculates the WENO weights based on the data  */
/* in the input array f                           */

int WENOWeights(Vec F, void *S, int scheme)
{
  if (!S) {
    printf("Error: in WENOWeights() - NULL pointer ");
    printf("passed as input for solver parameters.\n");
    return(1);
  }
  WENOParameters	*p = (WENOParameters*) S;
  int		       i, is, ie;
  double      *f,*w1,*w2,*w3;

  DM da;
  VecGetDM(F,&da);
  DMDALocalInfo da_info;
  DMDAGetLocalInfo(da, &da_info);
  is = da_info.xs;
  ie = da_info.xs + da_info.xm;

  DMDAVecGetArray(da,F,&f);
  DMDAVecGetArray(da,p->w1,&w1);
  DMDAVecGetArray(da,p->w2,&w2);
  DMDAVecGetArray(da,p->w3,&w3);
  for (i=is; i<ie+1; i++) {
    /* Defining stencil points */
    double m3, m2, m1, p1, p2;
    m3 = f[i-3];
    m2 = f[i-2];
    m1 = f[i-1];
    p1 = f[i];
    p2 = f[i+1];

    /* Candidate stencils and their optimal weights*/
    double c1, c2, c3;
    if (scheme == _FIFTH_ORDER_WENO_) {
      c1 = 0.3;
      c2 = 0.6;
      c3 = 0.1;
    } else if (scheme == _FIFTH_ORDER_CRWENO_) {
      c1 = 0.3;
      c2 = 0.5;
      c3 = 0.2;
    } else {
      printf("Error: in WENOWeights() - Argument 3 ");
      printf("is not a WENO or CRWENO scheme.      ");
      return(1);
    }

    if (p->no_limiting) {
      w1[i] = c1;
      w2[i] = c2;
      w3[i] = c3;
    } else {

      /* Smoothness indicators */
      double b1, b2, b3;
      b1 = thirteen_by_twelve*(m1-2*p1+p2)*(m1-2*p1+p2)
           + one_fourth*(3*m1-4*p1+p2)*(3*m1-4*p1+p2);
      b2 = thirteen_by_twelve*(m2-2*m1+p1)*(m2-2*m1+p1)
           + one_fourth*(m2-p1)*(m2-p1);
      b3 = thirteen_by_twelve*(m3-2*m2+m1)*(m3-2*m2+m1) 
           + one_fourth*(m3-4*m2+3*m1)*(m3-4*m2+3*m1);
  
      /* This parameter is needed for Borges' or Yamaleev-Carpenter
         implementation of non-linear weights or to obtain a fifth-
         order interpolation scheme with no limiting. */
      double tau;
      if (p->borges) {
        tau = absolute(b3 - b1);
      } else if (p->yc) {
        tau = (m3-4*m2+6*m1-4*p1+p2)*(m3-4*m2+6*m1-4*p1+p2);
      } else {
        tau = 0;
      }
  
      /* Definiting the non-linear weights */
      double a1, a2, a3;
      if (p->borges || p->yc) {
        a1 = c1 * (1.0 + raiseto(tau/(b1+p->eps),p->p));
        a2 = c2 * (1.0 + raiseto(tau/(b2+p->eps),p->p));
        a3 = c3 * (1.0 + raiseto(tau/(b3+p->eps),p->p));
      } else {
        a1 = c1 / raiseto(b1+p->eps,p->p);
        a2 = c2 / raiseto(b2+p->eps,p->p);
        a3 = c3 / raiseto(b3+p->eps,p->p);
      }

      /* Convexity */
      double a_sum_inv;
      a_sum_inv = 1.0 / (a1 + a2 + a3);
      w1[i] = a1 * a_sum_inv;
      w2[i] = a2 * a_sum_inv;
      w3[i] = a3 * a_sum_inv;
  
      /* Mapping the weights, if needed */
      if (p->mapped) {
        a1 = w1[i] * (c1 + c1*c1 - 3*c1*w1[i] + w1[i]*w1[i]) / (c1*c1 + w1[i]*(1.0-2.0*c1));
        a2 = w2[i] * (c2 + c2*c2 - 3*c2*w2[i] + w2[i]*w2[i]) / (c2*c2 + w2[i]*(1.0-2.0*c2));
        a3 = w3[i] * (c3 + c3*c3 - 3*c3*w3[i] + w3[i]*w3[i]) / (c3*c3 + w3[i]*(1.0-2.0*c3));
        a_sum_inv = 1.0 / (a1 + a2 + a3);
        w1[i] = a1 * a_sum_inv;
        w2[i] = a2 * a_sum_inv;
        w3[i] = a3 * a_sum_inv;
      }
  
    }
  }
  DMDAVecRestoreArray(da,p->w1,&w1);
  DMDAVecRestoreArray(da,p->w2,&w2);
  DMDAVecRestoreArray(da,p->w3,&w3);
  DMDAVecRestoreArray(da,F,&f);

  return(0);
}
