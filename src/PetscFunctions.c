#include <stdio.h>
#include <constants.h>
#include <solutionmethods.h>
#include <linearadvection.h>

ErrorType CalculateFlux(DM,Vec,Vec,void*);
ErrorType WENOWeights(Vec,void*,Integer);

ErrorType PetscRHSFunction(TS ts, Real t, Vec U, Vec F, void* S)
{
  if (!S) {
    printf("Error: in PetscRHSFunction() - NULL pointer passed ");
    printf("as argument for solver parameters.\n");
    return(1);
  }
  SolverParameters	*solver = (SolverParameters*) S;
  ErrorType		ierr;
  Vec			    f;

  DM da;
  TSGetDM(ts,&da);

  /* Calculate cell-centered flux f(u) (in u_t + f(u)_x = 0) */
  DMCreateLocalVector(da,&f);
  ierr = CalculateFlux(da,U,f,solver);
  if (ierr) return(ierr);
  if ((solver->spatial_scheme == _FIFTH_ORDER_WENO_) || (solver->spatial_scheme == _FIFTH_ORDER_CRWENO_)) {
    ierr = WENOWeights(f,&solver->weno,solver->spatial_scheme);
    if (ierr) return(ierr);
  }

  /* Reconstruct the flux derivative */
  if (solver->ReconstructionLHS) {
    /* Compact scheme: Af_x = Bf */
    /* Calculate Right hand side */
    ierr = solver->ReconstructionRHS(f,solver->Bf,&solver->weno,solver->dx);
    ierr = solver->ReconstructionLHS(solver->A,&solver->weno,solver->LeftB,solver->RightB);
    if (ierr) return(ierr);
    KSPSetOperators(solver->ksp,solver->A,solver->A,SAME_NONZERO_PATTERN);
    KSPSolve(solver->ksp,solver->Bf,F);
  } else {
    /* Non-compact scheme: f_x = Bf */
    ierr = solver->ReconstructionRHS(f,F,&solver->weno,solver->dx);
    if (ierr) return(ierr);
  }

  /* Clean up */
  VecDestroy(&f);
  return(0);
}

ErrorType PetscIFunction(TS ts, Real t, Vec U, Vec U_t, Vec F, void* S)
{
  if (!S) {
    printf("Error: in PetscIFunction() - NULL pointer passed ");
    printf("as argument for solver parameters.\n");
    return(1);
  }
  SolverParameters	*solver = (SolverParameters*) S;
  ErrorType		ierr;
  Vec			    f,AU_t;

  DM da;
  TSGetDM(ts,&da);
  DMDALocalInfo info;
  DMDAGetLocalInfo(da,&info);
  Integer size = info.xm;

  /* Calculate cell-centered flux f(u) (in u_t + f(u)_x = 0) */
  DMCreateLocalVector(da,&f);
  ierr = CalculateFlux(da,U,f,solver);
  if (ierr) return(ierr);
  if ((solver->spatial_scheme == _FIFTH_ORDER_WENO_) || (solver->spatial_scheme == _FIFTH_ORDER_CRWENO_)) {
    ierr = WENOWeights(f,&solver->weno,solver->spatial_scheme);
    if (ierr) return(ierr);
  }

  /* Reconstruct the flux derivative f_x                              */
  DMCreateGlobalVector(da,&AU_t);
  if (solver->ReconstructionLHS) {
    ierr = solver->ReconstructionLHS(solver->A,&solver->weno,solver->LeftB,solver->RightB);
    ierr = MatMult(solver->A,U_t,AU_t);
    if (ierr) return(ierr);
  } else {
    VecCopy(U_t,AU_t);
  }
  ierr = solver->ReconstructionRHS(f,F,&solver->weno,solver->dx);
  if (ierr) return(ierr);

  /* F(u,u_t) = Au_t - Bf */
  ierr = VecAYPX(F,-1.0,AU_t); 
  if (ierr) return(ierr);

  /* Clean up */
  VecDestroy(&f);
  VecDestroy(&AU_t);
  return(0);
}

ErrorType PetscIJacobian(TS ts,Real t,Vec U,Vec U_t,Real a,Mat *A,Mat *B,MatStructure *struc,void *S)
{
  ErrorType ierr;
  DM da;
  TSGetDM(ts,&da);
  if (!S) {
    printf("Error: in PetscIJacobian() - NULL pointer passed ");
    printf("as argument for solver parameters.\n");
    return(1);
  }
  SolverParameters *p = (SolverParameters*) S;
  p->shift = a;

  Vec f;
  DMCreateLocalVector(da,&f);
  ierr = CalculateFlux(da,U,f,p);
  if (ierr) return(ierr);
  if ((p->spatial_scheme == _FIFTH_ORDER_WENO_) || (p->spatial_scheme == _FIFTH_ORDER_CRWENO_)) {
    ierr = WENOWeights(f,&p->weno,p->spatial_scheme);
    if (ierr) return(ierr);
  }
  VecDestroy(&f);
  return(0);
}

ErrorType PetscJacobianFunction(Mat Jacobian,Vec U,Vec R)
{
  ErrorType		ierr;
  SolverParameters	*solver;
  MatShellGetContext(Jacobian,&solver);
  if (!solver) {
    printf("Error: in PetscJacobianFunction() - could not retrieve ");
    printf("solver parameters from Mat.\n");
    return(1);
  }

  DM da;
  VecGetDM(U,&da);
  DMDALocalInfo info;
  DMDAGetLocalInfo(da,&info);
  Integer size = info.xm;

  /* Reconstruct the flux derivative */
  Vec AU;
  DMCreateGlobalVector(da,&AU);
  if (solver->ReconstructionLHS) {
    ierr = solver->ReconstructionLHS(solver->A,&solver->weno,solver->LeftB,solver->RightB);
    if (ierr) return(ierr);
    ierr = MatMult(solver->A,U,AU);
    if (ierr) return(ierr);
  } else {
    ierr = VecCopy(U,AU);
    if (ierr) return(ierr);
  }

  Vec f;
  DMCreateLocalVector(da,&f);
  ierr = CalculateFlux(da,U,f,solver);
  if (ierr) return(ierr);
  ierr = solver->ReconstructionRHS(f,R,&solver->weno,solver->dx);
  if (ierr) return(ierr);
  VecDestroy(&f);

  /* R = a*U - f(u) */
  ierr = VecAXPBY(R,solver->shift,-1.0,AU); 
  if (ierr) return(ierr);

  /* Clean up */
  VecDestroy(&AU);
  return(0);
}
